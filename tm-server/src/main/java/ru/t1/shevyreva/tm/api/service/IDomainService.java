package ru.t1.shevyreva.tm.api.service;

public interface IDomainService {

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadJsonDataFasterXml();

    void loadJsonDataJaxB();

    void saveJsonDataFasterXml();

    void saveJsonDataJaxB();

    void loadXmlDataFasterXml();

    void loadXmlDataJaxB();

    void saveXmlDataFasterXml();

    void saveXmlDataJaxB();

    void loadYamlDataFasterXml();

    void saveYamlDataFasterXml();
}

package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskCreateRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Create new task.";

    @NotNull
    private final String NAME = "task-create";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setDescription(description);
        request.setName(name);
        getTaskEndpoint().createTask(request);
    }

}

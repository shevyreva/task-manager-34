package ru.t1.shevyreva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskBindToProjectRequest(@Nullable final String taskId, @Nullable final String projectId) {
        this.taskId = taskId;
        this.projectId = projectId;
    }

    public TaskBindToProjectRequest(@Nullable String token) {
        super(token);
    }

}
